w = 800
h = 800
viewBox = "0 0 800 800"
preserveAspectRatio = "xMidYMid"
maxNodeSize = 50
x_browser = 20
y_browser = 25
root = undefined
vis = undefined
force = d3.layout.force()

teamgraph = $("#teamgraph")

vis = d3.select('#teamgraph').append('svg').attr('width', w).attr('height', h).attr('id', 'teamgraphsvg').attr('viewBox', viewBox).attr('preserveAspectRatio', preserveAspectRatio)

teamgraphsvg = $("#teamgraphsvg")
console.log teamgraphsvg

d3.json '/data/team.json', (json) ->
  root = json
  root.fixed = true
  root.x = w / 2
  root.y = h / 2
  defs = vis.insert('svg:defs').data([ 'end' ])
  defs.enter().append('svg:path').attr 'd', 'M0,-5L10,0L0,5'
  update()
  return

update = ->
  nodes = flatten(root)
  links = d3.layout.tree().links(nodes)

  tick = ->
    path.attr 'd', (d) ->
      dx = d.target.x - (d.source.x)
      dy = d.target.y - (d.source.y)
      dr = 0
      'M' + d.source.x + ',' + d.source.y + 'A' + dr + ',' + dr + ' 0 0,1 ' + d.target.x + ',' + d.target.y
    node.attr 'transform', nodeTransform
    return

  force.nodes(nodes).links(links).gravity(0.05).charge(-1500).linkDistance(150).friction(0.5).linkStrength((l, i) ->
    1
  ).size([
    w
    h
  ]).on('tick', tick).start()
  path = vis.selectAll('path.link').data(links, (d) ->
    d.target.id
  )

  path.enter().insert('svg:path').attr('class', 'link').style('stroke', '#fff').style('opacity',0.5)
  
  path.exit().remove()
  
  node = vis.selectAll('g.node').data(nodes, (d) ->
    d.id
  )

  nodeEnter = node.enter().append('svg:g').attr('class', 'node').attr('transform', (d) ->
    'translate(' + d.x + ',' + d.y + ')'
  ).on('click', click).call(force.drag)

  nodeEnter.append('svg:circle').attr('r', (d) ->
    Math.sqrt(d.size) / 10 or 4.5
  ).style('fill', '#ff9800').style('stroke', '#ffcc80').style('opacity', '0.5')

  logo = nodeEnter.append('svg:image').attr('xlink:href', (d) ->
    d.logo
  ).attr('x', (d) ->
    -40
  ).attr('y', (d) ->
    -40
  ).attr('height', 80).attr('width', 80)

  images = nodeEnter.append('svg:image').attr('xlink:href', (d) ->
    d.img
  ).attr('x', (d) ->
    -25
  ).attr('y', (d) ->
    -25
  ).attr('height', 50).attr('width', 50)

  setEvents = images.on('click', (d) ->
    if d.hero
      d3.select('.matename').html d.hero
      d3.select('.matetitle').html d.group
      d3.select('.matebio').html d.bio
    if d.link
      d3.select('.matelink').html '<a href=\'' + d.link + '\' >' + 'web page ⇢' + '</a>'
    else 
      d3.select('.matelink').html ''
    return
  ).on('mouseenter', ->
    d3.select(this).transition().attr('x', (d) ->
      -60
    ).attr('y', (d) ->
      -60
    ).attr('height', 100).attr 'width', 100
    return
  ).on('mouseleave', ->
    d3.select(this).transition().attr('x', (d) ->
      -25
    ).attr('y', (d) ->
      -25
    ).attr('height', 50).attr 'width', 50
    return
  )

  nodeEnter.append('text').attr('class', 'nametag').attr('x', -50).attr('y', 50).attr('fill', '#ff9800').attr("font-size", "30px").text (d) ->
    d.hero
    
  nodeEnter.append('text').attr('class', 'label').attr('x', -50).attr('y', 10).attr('fill', '#ffcc80').attr("font-size", "20px").text (d) ->
    d.name
    
  node.exit().remove()
  path = vis.selectAll('path.link')
  node = vis.selectAll('g.node')
  return

nodeTransform = (d) ->
  d.x = Math.max(maxNodeSize, Math.min(w - (d.imgwidth / 2 or 16), d.x))
  d.y = Math.max(maxNodeSize, Math.min(h - (d.imgheight / 2 or 16), d.y))
  'translate(' + d.x + ',' + d.y + ')'

click = (d) ->
  if d.children
    d._children = d.children
    d.children = null
  else
    d.children = d._children
    d._children = null
  update()
  return

flatten = (root) ->
  nodes = []
  i = 0

  recurse = (node) ->
    if node.children
      node.children.forEach recurse
    if !node.id
      node.id = ++i
    nodes.push node
    return

  recurse root
  nodes

  
$(window).on('resize', ->
  targetWidth = $("#teambox").width()
  targetHeight = $("#teambox").height()
  teamgraphsvg.attr 'width', targetWidth
  teamgraphsvg.attr 'height', targetHeight
  return
  ).trigger 'resize'


# ---
# generated by js2coffee 2.0.4
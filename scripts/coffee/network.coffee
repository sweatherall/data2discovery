width = 960
height = 500
viewBox = "0 0 960 500"
preserveAspectRatio = "xMidYMid"
aspect = width / height
network = $("#network")

force = d3.layout.force().linkDistance(20).linkStrength(1).size([
  width
  height
])

dragstart = (d) ->
  d3.select(this).classed("colored", true)

drag = force.drag().on("dragstart", dragstart)

svg = d3.select(network.selector).append('svg').attr('width', width).attr('height', height).attr('viewBox', viewBox).attr('preserveAspectRatio', preserveAspectRatio).attr('id', 'networksvg')

networksvg = $("#networksvg")
console.log networksvg

d3.json '/data/miserables.json', (error, graph) ->
  if error
    throw error
  nodes = graph.nodes.slice()
  links = []
  bilinks = []
  graph.links.forEach (link) ->
    s = nodes[link.source]
    t = nodes[link.target]
    i = {}
    # intermediate node
    nodes.push i
    links.push {
      source: s
      target: i
    },
      source: i
      target: t
    bilinks.push [
      s
      i
      t
    ]
    return

  force.nodes(nodes).links(links).start()
  
  link = svg.selectAll('.link').data(bilinks).enter().append('path').attr('class', 'link')
  
  node = svg.selectAll('.node').data(graph.nodes).enter().append('circle').attr('class', 'node').attr('r', (d) ->
    if d.group <= 5
      d.group+5
    else d.group-5
  ).style('fill', (d) ->
    if d.group is 0 or d.group is 1
      dg = 2
    else dg = d.group
    alpha = .1 * dg
    rgba = "rgba(255,255,255," + alpha + ")"
    rgba
  ).call(drag)

  node.append('title').text (d) ->
    d.name
    
  force.on 'tick', ->
    link.attr 'd', (d) ->
      'M' + d[0].x + ',' + d[0].y + 'S' + d[1].x + ',' + d[1].y + ' ' + d[2].x + ',' + d[2].y
    node.attr 'transform', (d) ->
      'translate(' + d.x + ',' + d.y + ')'
    return
  return


$(window).on('resize', ->
  targetWidth = $("#index-banner").width()
  targetHeight = $("#index-banner").height()
  networksvg.attr 'width', targetWidth
  networksvg.attr 'height', targetHeight
  return
  ).trigger 'resize'
$(document).ready ->
  
  # Materialize JS initializations
  $(".dropdown-button").dropdown()
  $(".button-collapse").sideNav()
  
  
  
  #  smooth scroll
  $('a[href*=#]:not([href=#])').click ->
    if location.pathname.replace(/^\//, '') == @pathname.replace(/^\//, '') and location.hostname == @hostname
      target = $(@hash)
      target = if target.length then target else $('[name=\' + this.hash.slice(1) + \']')
      if target.length
        $('html,body').animate { scrollTop: target.offset().top - 60}, 500
        false
  
  

  # initial styles for 'unique' description
  $('.choice-description.semap').addClass('active')
  $('.choice-title.semap').addClass 'active'
  $('.image-container.semap').addClass 'active'
  
  # onclick styles for 'unique' description
  $('.choice').click ->
    $('.choice-description').toggleClass('active')
    $('.choice-title').toggleClass('active')
    $('.image-container').toggleClass('active')
    
    
    
  #  'Our Workflow' - animated entrance
  $(window).scroll ->
    $('.steps').each ->
      stepsPos = $(this).offset().top
      topOfWindow = $(window).scrollTop()
      if stepsPos < topOfWindow + 600
        console.log "steps is < topofwindow"
        setTimeout (->
          $('.step1 ').addClass 'scaleIn'
          setTimeout (->
            $('.step2').addClass 'scaleIn'
            setTimeout (->
              $('.step3').addClass 'scaleIn'
              setTimeout (->
                $('.circlenum.one').addClass 'active'
                $('.step1 .step').addClass 'active'
                setTimeout (->
                  $('.circlenum.two').addClass 'active'
                  setTimeout (->
                    $('.circlenum.three').addClass 'active'
                    setTimeout (->
                      $('.row.currently').addClass 'scaleIn'
                    ), 500
                  ), 450
                ), 400
              ), 350
            ), 250
          ), 150
        ), 50




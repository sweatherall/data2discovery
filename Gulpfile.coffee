gulp = require 'gulp'
less = require 'gulp-less'
coffee = require 'gulp-coffee'
jade = require 'gulp-jade'
concat = require 'gulp-concat'
minifyCss = require 'gulp-minify-css'


## DEFAULT
gulp.task 'default', () ->
  ['jade','less','coffee']
    
    
## WATCH
gulp.task 'watch', () ->
    gulp.watch 'views/jade/*.jade', ['jade']
    gulp.watch 'styles/less/*.less', ['less']
    gulp.watch 'scripts/coffee/*.coffee', ['coffee']
    
  

## JADE --> HTML
gulp.task 'jade', () ->
  gulp.src [
    'views/jade/*.jade'
  ]
    .pipe jade({pretty: true})
    .pipe gulp.dest('views/html/')
    
    
## LESS --> CSS
gulp.task 'less', () ->
  gulp.src [
    'styles/less/styles.less'
  ]
    .pipe less()
#    .pipe concat('styles.css')
#    .pipe minifyCss()
    .pipe gulp.dest('styles/')
    
    
## COFFEE --> JS
gulp.task 'coffee', () ->
  gulp.src [
    'scripts/coffee/*.coffee'
  ]
    .pipe coffee()
    .pipe concat('main.js')
    .pipe gulp.dest('scripts/js')
    
    
## VENDOR FILES
gulp.task 'vendorjs', () ->
  gulp.src [
    'bower_components/jquery/dist/jquery.min.js'
    'node_modules/bootstrap/dist/js/bootstrap.min.js'
    'node_modules/materialize-css/bin/materialize.js'
    'bower_components/d3/d3.min.js'
  ]
    .pipe concat('vendor.js')
    .pipe gulp.dest('scripts/vendor')
    
    